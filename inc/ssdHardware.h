/*!
 *  @file 	   ssdHardware.h
 *  @brief     SSD1306 128x64 LCD library.
 *  @details   Hardware dependencies needed for I2C communication. This header file can be used
 *  		   in application defined hardware initialization for I2C.
 *
 *  @warning   Needs to be edited by the user accordingly.
 *  @author    Federico Baigorria
 *  @date      03-24-2020
 *  @copyright GNU Public License.
 */

#ifndef __SSD1306_INC_SSDHARDWARE_H__
#define __SSD1306_INC_SSDHARDWARE_H__

/* Include here all the board and hardware header files needed for SPI peripheral
 * control and communication
 */

#include "board.h"
#include "i2c_17xx_40xx.h"

///! Hardware macro definitions
#define SSD_I2C		I2C0			///! I2C ID Port to be used


#endif /* __SSD1306_INC_SSDHARDWARE_H__ */
